# Atelier 2

Créer un panier de formulaire pour entrer les informations de l'utilisateur:
  * Nom et prénom
  * Date de naissance
  * Sexe
  * Adresse

Veuillez vérifier le contenu de chaque case et afficher des messages d'erreur appropriés:
  - Nom et prénom (minimum de 3 caractères chaques et un espace entre les deux.) Il ne faut pas deux cases, juste une.
  - Âgé de plus de 18 ans pour compléter le formulaire.
  - Adresse existante (utiliser le module Geocode (https://pub.dev/packages/geocode) pour valider l'adresse.
  - Afficher la météo associée à l'adresse de la personne pour les 5 prochains jours en utilisant les icônes appropriées représentant la météo.