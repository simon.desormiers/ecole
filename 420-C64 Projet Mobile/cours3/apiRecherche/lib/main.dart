import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'models/user.dart';
import 'dart:convert';
import 'dart:math';

void main() {
  runApp(const MonAppRecherche());
}

class MonAppRecherche extends StatelessWidget {
  const MonAppRecherche({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const MonAppRechercheFul(title: 'Recherche utilisateur'),
    );
  }
}

class MonAppRechercheFul extends StatefulWidget {
  const MonAppRechercheFul({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MonAppRechercheFul> createState() => _MonAppRechercheFul();
}

class _MonAppRechercheFul extends State<MonAppRechercheFul> {
  late Future<user>? _user;
  String? gender;

  @override
  void initState() {
    super.initState();
    _user = null;
    gender = null;
  }

  void _onPressed() {
    String apiGender = gender == "Homme" ? "male" : "female";
    _user = null;
    setState(() => {
      _user = fetchUser(apiGender)
    });
  }

  Future<user> fetchUser(String apiGender) async {
    await Future.delayed(const Duration(milliseconds:5000));
    final response = await http
        .get(Uri.parse('https://randomuser.me/api/?gender=' + apiGender));

    if (response.statusCode == 200) {
      final json = response.body;
      final extractedData = jsonDecode(json);

      List users = extractedData["results"];
      return user.fromJson(users[0]);
    } else {
      throw Exception(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            DropdownButton<String>(
              value: gender,
              icon: const Icon(Icons.arrow_downward),
              elevation: 16,
              style: const TextStyle(color: Colors.blueGrey),
              underline: Container(
                height: 2,
                color: Colors.blueGrey,
              ),
              onChanged: (String? newValue) {
                setState(() {
                  gender = newValue;
                });
              },
              items: <String>['Homme', 'Femme']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
            TextButton(
                child: const Icon(
                  Icons.person_search,
                ),
                onPressed: _onPressed),
          ]),
          Card(
            shape: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: const BorderSide(color: Colors.blueGrey),
            ),
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: FutureBuilder<user>(
                  future: _user,
                  builder: (context, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                      case ConnectionState.waiting:
                        return Column(children: const [
                          SizedBox(
                            width: 60,
                            height: 60,
                            child: CircularProgressIndicator(),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 16),
                            child: Text('Attente des données...'),
                          ),
                        ]);
                      default:
                        if (snapshot.hasData) {
                          return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 25),
                                  child:
                                  Image.network(snapshot.data!.pictures.large),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 10),
                                      child: SizedBox(
                                        width: 150,
                                        child: Text(
                                            snapshot.data!.yourname.toString(),
                                            style: const TextStyle(
                                                fontSize: 25)),
                                      ),
                                    ),
                                    Text(snapshot.data!.email),
                                    Text(snapshot.data!.cell)
                                  ],
                                ),
                              ]);
                        }
                        else {
                          return Column(children: const [
                            SizedBox(
                              width: 60,
                              height: 60,
                              child: CircularProgressIndicator(),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 16),
                              child: Text('Erreur...'),
                            ),
                          ]);
                        }
                    }
                  }),
            ),
          ),
        ]),
      ),
    );
  }
}
