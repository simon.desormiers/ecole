# Flutter

* Flutter est un framework permettant de compiler pour iOS, Android, Linux et Web avec le même code source. Lorsque vous créez vos projets dans intelliJ idea, vous aurez le choix pour quel plateforme devra être compilé votre application.

* Flutter est relativement récent (2018) et peu utilisé (200 millions d'utilisateurs).

* 50 000 applications Flutter sur le Play Store

* 500 000 développeurs

* Supporté par Google.


## Pourquoi les gens sont intéressés par Flutter?

### Fuschia OS

Un nouveau système d'exploitation par Google: Fuchia OS, devrait ultimement remplacé Android (basé sur Linux). L'avantage c'est qu'il ne roule pas uniquement sur les téléphones. Flutter sera le langage de prédilection pour cette plateforme en plus qu'il sera utilisé pour le rendu des applications.

### Dart
Le langage de programmation Dart a été dévoilé par Google à la conférence GOTO en
2011, et Dart 1.0 fut offert à la fin de 2013. Initialement reconnu comme un remplaçant de Javascript, la portion de développeur Dart est restée très basse. Cependant depuis l'arrivée de Flutter, Dart est devenu largement plus utilisé.

### Pourquoi flutter a choisi DART?

Le but de flutter a toujours été d'offrir une alternative rapide au cadriciel mutli-plateforme actuel. Mais pas seulement ça, d'offrir au développeur mobile une expérience de développement améliorée:

* La compilation dart permet de choisir entre 

  * AOT (Ahead of time): lorsque la performance est nécessaire et que l'on produit une release.

  * JIT (Just in time): lorsqu'on est en développement: il n'est pas nécessaire de recompiler au complet pour voir le changement.

* Bonne performance: Puisque Dart supporte la compilation AOT, Flutter est responsive et alloue un démarrage rapide.
  
* Garbage collection: Flutter utilise un flow fonctionnel et des objets de courtes-vies. Le garbage collector de Dart fonctionne sans locks.
  
* Facile à apprendre: Dart est un langage flexible, robuste, moderne et avancé.
  
* UI intéractif: En Flutter, on déclare l'interface de nos widgets (immuables). Pour tout changement d'interface, une méthode build permet de recréer les objets.

## Outil de ligne de commande

flutter vient avec un logiciel de ligne de commande vous permettant certaines fonctionnalités: 

![](images/Flutter.png)

Les commandes principales pour vous aider à partir:

* create: permet de créer un projet flutter
* doctor: permet de savoir si votre installation de flutter est fonctionnelle.
* logs: permet de voir les messages en console
* pub: permet de gérer les packets
* run: permet de lancer l'exécution
* emulators: permet de lancer un émulateur.
* install: permet d'installer sur un émulateur
* test: permet de lancer les tests unitaires de votre projet en cours.
  
### iOS

Vous devez utiliser un ordinateur mac pour compiler sur mac. Dans votre projet flutter créé, vous aurez un répertoire ios que vous pourriez ouvrir avec xcode pour la compilation d'un projet flutter.

## Émulateurs

Vous devez créer un émulateur pour vos projets flutter pour la session.

## Commande create

![](images/Projet%20flutter.png)

* android / ios: les répertoires respectifs des différents plateformes.

* lib: projet principal de flutter avec les différents fichiers .dart nécessaire pour votre projet.

* pubspec.xml: contient les informations de votre paquetage dart, tout comme les dépences externes, images, fonts et plus.

* README: contient l'information de votre projet.

* test: contient l'information pour les tests unitaires de votre projet.

## Tout est un widget, presque.

Les widgets Flutter sont partout dans l'application. Pe pas tout l'est mais presque tout l'est. Même l'application est un widget Flutter et c'est pour cette raison que le concept est si important.

Un widget représente une partie de l'UI, mais ça ne signifie pas que c'est seulement les trucs visibles.

Voici une liste de ce que peut être un widget:
* Un élément visuel ou de structure tel qu'un Button ou un Text.
* Un élement de layout définissant la position, la marge, le padding, tel que
le Padding widget.
* Un élément de style servant à colorier ou appliquer un theme à un élément
* Un élément d'intéraction qui répond à des intéractions utilisateurs tel que le GestureDetector.

Voici un exemple d'un widget:

```dart
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
    Widget build(BuildContext context) {
        return MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
            primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(title: 'Flutter Demo Home Page'),
        );
    }
}
```

Un point important est de noter le build, qui permet de mettre à jour l'interface qui est appelé lorsque des activités externes se produisent (intéraction utilisateur, données provenant d'une bd, timer expire, ...)

Dans le cas présent, le MyApp retourne un autre widget, MaterialApp (définit ailleurs), qui a également un build et qui peut retourner un widget.

Les widgets sont les blocs légos qui bâtissent une interface:

![](images/Widget.png)

L'arbre pour bâtir ce widget est le suivant:

![](images/Arbre%20widget.png)

### Stateless

Une interface sera composée de plusieurs widgets, plusieurs ne changeront jamais tout au long de l'application. Ceux-ci sont nommés Stateless widgets. 

Ils n'ont pas d'état et ne se modifient pas suite à des événements externes. Leur parent crée l'objet stateless avec ses propriétés and ne changeront jamais par eux-mêmes, ils sont constants tout le long de l'application.

```dart

class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
            primarySwatch: Colors.blue,
            ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
        );
    }
}
```

### Stateful

Vous aurez donc compris que les Stateful object ont des états changeants au travers de l'éxécution. Il faut comprendre aussi que le widget est également Stateless mais ils ont une classe compagnon **State** qui elle représente l'état actuel du widget.

```dart

class MyHomePage extends StatefulWidget {
    MyHomePage({Key key, required this.title}) : super(key: key);
    final String title;
    
    @override
    _MyHomePageState createState() => _MyHomePageState();
}
```

Premièrement, on remarque la présence de StatefulWidget, identifiant qu'il va y avoir un objet State. Les objets Stateful doivent overrider la méthode createState() afin de créer localement un objet ici _MyHomePageState() qui va être un objet que l'on décrit ultérieurement.

```dart

class _MyHomePageState extends State<MyHomePage> {
    int _counter = 0;
    void _incrementCounter() {
        setState(() {
            _counter++;
        });
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Text(widget.title),
            ),
            body: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        Text(
                            'You have pushed the button this many times:',
                        ),
                        Text(
                            '$_counter',
                            style: Theme.of(context).textTheme.headline4,
                        ),
                    ],
                ),
            ),
            floatingActionButton: FloatingActionButton(
                onPressed: _incrementCounter,
                tooltip: 'Increment',
                child: Icon(Icons.add),
            ), // This trailing comma makes auto-formatting nicer for build methods.
        );
    }
}
```

Allons y en ordre:

```dart
void _incrementCounter() {
    setState(() {
        _counter++;
    });
}
```

La première méthode que l'on rencontre est la méthode setState, qui ne reçoit aucun argument et qui ne retourne rien, mais elle ne fait pas rien. 

Un stateful widget est défini tel qu'il va changer son apparence pendant son exécution et définit ce qui va changer. Comment le framework va t'il être mis au courant que qqch à changer dans son exécution? La méthode **setState**. Cette méthode reçoit une fonction en paramètre, une fonction anonyme qui incrémente la valeur counter. Lorsqu'appelé, le widget sera redessiné avec la nouvelle valeur déjà définie. 


## Liste de widgets traditionnels

https://docs.flutter.dev/development/ui/widgets

## Dart

Lecture sur dart recommandée:

https://dart.dev/guides/language/language-tour


## Dart Future

Lorsque une méthode appelle une tâche qui peut être très longue, nous ne voulons pas bloquer l'exécution de notre interface. Pour spécifier qu'une méthode est asynchrone, nous pouvons utiliser le mot-clé async indiquant que vous ne devriez pas bloquer l'exécution en attendant les résultats.

Cependant, lors d'appel d'API ou de lecture d'une BD, nous pourrions désirer d'avoir une façon de revenir à la méthode lorsque l'information est présente. Pour faire cela spécifiquement, nous utilisons le await. Une grande distinction entre les deux est que la méthode est déclarée async, mais que c'est le code qui va indiqué où recommencée lorsqu'on utilise await.

Bien que nous ayons fait cela, qu'est-ce que la méthode retourne??? Le object Future<T> spécifie une valeur qui sera définie dans le futur.

Soit un exemple:

```dart

import 'dart:io';
void longRunningOperation() {
    for (int i = 0; i < 5; i++) {
        sleep(Duration(seconds: 1));
        print("index: $i");
    }
}

main() {
    print("start of long running operation");
    longRunningOperation();
    print("continuing main body");
    for (int i = 10; i < 15; i++) {
        sleep(Duration(seconds: 1));
        print("index from main: $i");
    }
    print("end of main");
}
```

Va afficher:

```
start of long running operation
index: 0
index: 1
index: 2
index: 3
index: 4
continuing main body
index from main: 10
index from main: 11
index from main: 12
index from main: 13
index from main: 14
end of main
```

On note que l'application est arrêtée pendant l'exécution de longRunningOperation. C'est une exécution synchrone.

Si ça l'avait été une application complète, l'interface aurait été bloquée le temps que l'opération longRunningOperation soit complétée.

Adaptons le code conséquemment:

```dart

import 'dart:io';
import 'dart:async';

Future<void> longRunningOperation() async {
    for (int i = 0; i < 5; i++) {
        sleep(Duration(seconds: 1));
        print("index: $i");
    }
}

main() { ... } // identique
```

Remarquez les différences, dans l'utilisation du Future. Pour l'utilisation de l'async, il a fallu utiliser le module async. De plus, nous ne retournons rien avec notre méthode Future, donc on le garde de type void.

Le résultat de l'application:

```
start of long running operation
index: 0
index: 1
index: 2
index: 3
index: 4
continuing main body
index from main: 10
index from main: 11
index from main: 12
index from main: 13
index from main: 14
end of main
```

Exactement la même chose, étrange.

Le code s'exécute comme précédemment, l'appel à la méthode longRunningOperation dans le main s'effectue et la méthode se met en Future.Delayed (attente). 

La fonction main s'exécute avec un sleep qui est, on le rappelle synchrone donc le thread ne peut jamais être libéré de la méthode main.

Afin de corriger le tout, nous devons également rendre le thread main() async.

```dart
main() async {
    print("start of long running operation");
    longRunningOperation();
    print("continuing main body");

    for (int i = 10; i < 15; i++) {
        await Future.delayed(Duration(seconds: 1));
        print("index from main: $i");
    }
    print("end of main");
}
```

L'application va afficher alors:

```
start of long running operation
continuing main body
index: 0
index from main: 10
index: 1
index from main: 11
index: 2
index from main: 12
index: 3
index from main: 13
index: 4
index from main: 14
end of main
```

