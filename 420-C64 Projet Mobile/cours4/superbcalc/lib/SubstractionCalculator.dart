
import 'package:flutter/material.dart';


class SubstractionCalculator extends StatefulWidget {
  const SubstractionCalculator({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<SubstractionCalculator> createState() => _SubstractionCalculator();
}

class _SubstractionCalculator extends State<SubstractionCalculator> {
  TextEditingController controllerSubs1 = TextEditingController();
  TextEditingController controllerSubs2 = TextEditingController();
  int substract = 0;

  void _recalculateNumber(String text) {
    setState(() {
      var number1 = int.tryParse(controllerSubs1.text) ?? 0;
      var number2 = int.tryParse(controllerSubs2.text) ?? 0;

      substract = number1 - number2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextFormField(
            controller: controllerSubs1,
            keyboardType: TextInputType.number,
            onChanged: _recalculateNumber,
          ),
          TextFormField(
            controller: controllerSubs2,
            keyboardType: TextInputType.number,
            onChanged: _recalculateNumber,
          ),
          Text(
            '$substract',
            style: Theme.of(context).textTheme.headline4,
          ),
        ],
      );
  }
}