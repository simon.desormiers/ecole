import 'package:flutter/material.dart';
import 'AdditionCalculator.dart';
import 'SubstractionCalculator.dart';
import 'MultiplicationCalculator.dart';
import 'DivisionCalculator.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyFullWidget(),
      );
  }
}

class MyFullWidget extends StatelessWidget {
  const MyFullWidget({super.key});

  @override
  Widget build (BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Super Dooper Cool Calculator"),
      ),
      body:
          Center( child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded( child:
                  Center(child:
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                      Expanded(
                        child: AdditionCalculator(title:'Addition'),
                      ),
                      Expanded(
                        child: SubstractionCalculator(title: 'Substraction'),
                      )
                    ])
                  ),
                ),
                Expanded( child:
                  Center(child:
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                      Expanded(
                        child: MultiplicationCalculator(title: 'Multiplication'),
                      ),
                      Expanded(
                        child: DivisionCalculator(title: "Division"),
                      )
                    ]),
                  ),
                ),
          ]),
      )
    );
  }
}