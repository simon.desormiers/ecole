
import 'package:flutter/material.dart';


class DivisionCalculator extends StatefulWidget {
  const DivisionCalculator({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<DivisionCalculator> createState() => _DivisionCalculator();
}

class _DivisionCalculator extends State<DivisionCalculator> {
  TextEditingController controllerDiv1 = TextEditingController();
  TextEditingController controllerDiv2 = TextEditingController();
  double div = 0;

  void _recalculateNumber(String text) {
    setState(() {
      var number1 = int.tryParse(controllerDiv1.text) ?? 0;
      var number2 = int.tryParse(controllerDiv2.text) ?? 0;

      div = number1 / number2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextFormField(
            controller: controllerDiv1,
            keyboardType: TextInputType.number,
            onChanged: _recalculateNumber,
          ),
          TextFormField(
            controller: controllerDiv2,
            keyboardType: TextInputType.number,
            onChanged: _recalculateNumber,
          ),
          Text(
            '$div',
            style: Theme.of(context).textTheme.headline4,
          ),
        ],
      );
  }
}