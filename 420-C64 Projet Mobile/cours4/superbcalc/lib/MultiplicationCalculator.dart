
import 'package:flutter/material.dart';


class MultiplicationCalculator extends StatefulWidget {
  const MultiplicationCalculator({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MultiplicationCalculator> createState() => _MultiplicationCalculator();
}

class _MultiplicationCalculator extends State<MultiplicationCalculator> {
  TextEditingController controllerMul1 = TextEditingController();
  TextEditingController controllerMul2 = TextEditingController();
  int multiply = 0;

  void _recalculateNumber(String text) {
    setState(() {
      var number1 = int.tryParse(controllerMul1.text) ?? 0;
      var number2 = int.tryParse(controllerMul2.text) ?? 0;

      multiply = number1 * number2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextFormField(
            controller: controllerMul1,
            keyboardType: TextInputType.number,
            onChanged: _recalculateNumber,
          ),
          TextFormField(
            controller: controllerMul2,
            keyboardType: TextInputType.number,
            onChanged: _recalculateNumber,
          ),
          Text(
            '$multiply',
            style: Theme.of(context).textTheme.headline4,
          ),
        ],
      );
  }
}