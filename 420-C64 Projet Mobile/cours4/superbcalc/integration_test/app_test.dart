import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'package:superbcalc/main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets('addition de 2 nombres', (tester) async {
    await tester.pumpWidget(const app.MyApp());
    await tester.pumpAndSettle();

    final inputField1 = find.byKey(const ValueKey("textAdd1"));
    final inputField2 = find.byKey(const ValueKey("textAdd2"));
    final result = find.byKey(const ValueKey("resultAdd"));

    await tester.tap(inputField1);
    await tester.enterText(inputField1, "100");

    await tester.tap(inputField2);
    await tester.enterText(inputField2, "-100");
    await tester.pump(const Duration(milliseconds: 400));

    var text = result.evaluate().single.widget as Text;
    expect (text.data, "0");

  });

}
