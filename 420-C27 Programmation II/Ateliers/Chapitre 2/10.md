# Atelier 10

Dans un nouveau projet créer un fichier Villes.java 
Écrire les méthodes utilitaires suivantes (aucune méthode main):

1.	afficherTab : Affiche à la console un tableau 2D d'entiers ligne par ligne avec en espace entre chaque élément.

2.	sommeUneLigne : Retourne la somme d'une ligne d'un tableau 2D d'entiers.

3.	moyenneUneLigne : Retourne la moyenne d'une ligne d'un tableau 2D d'entiers.

4.	maxLigne : Retourne la valeur maximum d'une ligne d'un tableau 2D d'entiers.

5.	minLigne : Retourne la valeur minimum d'une ligne d'un tableau 2D d'entiers.

Créer un fichier de test VillesTest qui teste les méthodes 2, 3, 4 et 5.

Déclarer le tableau suivant représentant les précipitations reçues pour quatre villes durant une année:

![](img/Tableau2D.png)

Déclarer un tableau de String pour le nom des villes.

Pour les actions suivantes faire appel aux méthodes implémentées dans Villes.java

a)	Afficher le tableau de précipitation, une ligne par ville.
```
76 68 81 91 65 96 107 94 98 107 96 130 
55 51 59 65 66 67 69 82 72 61 72 67 
71 63 70 72 76 90 89 96 89 80 92 92 
87 68 84 84 78 78 95 91 73 89 86 102
```

b)	Afficher les précipitations totales (la somme des précipitations) de toute l'année pour chaque ville.
```
Total Québec: 1109
Total Toronto: 786
Total Joliette: 980
Total Gaspé: 1015
```

c)	Afficher la moyenne des précipitations annuelles par ville sur deux décimales.
```
Moyenne Québec 92,42
Moyenne Toronto 65,50
Moyenne Joliette 81,67
Moyenne Gaspé 84,58
```

d)	Afficher la quantité maximale de précipitation pour chaque ville dans une année.
```
Max Québec 130
Max Toronto 82
Max Joliette 96
Max Gaspé 102
```

e)	Afficher la quantité minimale de précipitation pour chaque ville dans une année.
```
Min Québec 65
Min Toronto 51
Min Joliette 63
Min Gaspé 68
```