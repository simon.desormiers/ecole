# Gestionnaire d'événement

En javaFX, nous pouvons développer des applications graphiques (GUI). Dans ses applications, lorsqu'un utilisateur interagit avec les nodes de l'application, un événement est créé et doit être géré.

## Type d'événement

* Avant-plan (Foreground): Ils proviennent avec une intéraction directe avec l'utilisateur: un clic sur un bouton, choisir un élément dans une liste, scroller une page, entrer un caractère au clavier.

* Arrière-plan (Background): Ils dépendent plus du système d'exploitation que de l'utilisateur. 

## Multitudes d'événements gérables en javaFX:

Voici une liste incomplète des types d'événements gérables en javaFX:

* ActionEvent - Type d'événement lorsqu'un bouton d'interface est cliqué
* MouseEvent - Type d'événement qui survient lorsqu'un bouton (de la souris). Plusieurs potentiels: cliqué, pressé, relâché, déplacé, entre dans une zone, sort d'une zone, ...
* KeyEvent - Type d'événement qui survient lorsqu'un bouton (du clavier) survient sur une node. Plusieurs potentiels: touche pesée, touche relâchée, type de touche.
* DragEvent - Type d'événement qui survient lors d'un glissement d'un objet. Supporte ce qu'on appelle le Drag & Drop, ...
* Window Event - Type d'événement qui survient par rapport à la gestion de la fenêtre. Affiché, cachée, ...

## Gestion des événements

La gestion des événements est le mécanisme qui contrôle et décide ce qui devrait subvenir si un événement survient. Le mécanisme a le code nécessaire à faire exécuter lorsqu'un événement survient.

JavaFX fournit les *handlers* et les *filters* nécessaire pour les gérer. En javaFX, tous les événements ont:

* Un type: Qu'est-ce qui s'est produit? Dans le cas d'un mouseEvent, ça peut être pressée, relâchée, ...
* Une source: Qu'est-ce qui produit l'événement. C'est l'origine de l'événement. Dans le cas d'un mouseEvent, c'est la souris qui est la source.
* Une cible: Sur quoi subvient l'événement. Ça peut être une node, une scène ou une fenêtre.

## Les étapes de la gestion en javaFX

1. Construction de la route
   
La chaîne de gestion est crée afin de déterminer le chemin approprié pour un événement, peu importe quand il est généré. Il descend à partir du stage vers le node sur lequel l'événement est généré.

![](images/javafx-event-handling.png)

2. Capture de l'événement

L'événement est émis à partir du haut vers la cible (node). Si un des nodes sur le chemin de la destination a le filtre, le filtre est appelé et l'événement est passé au suivant. Si aucun filtre ne consomme l'événement, la cible va le recevoir éventuellement et le processer.

3. Les bulles de l'événement

Lorsque la cible est rejointe, l'événement fait le chemin inverse et part de la cible et remonte vers la racine de l'application. Si un des nodes a un gestionnaire implémenté pour gérer l'événement, il est appelé. Lorsque complété, l'événement continue son petit bonhomme de chemin vers le haut de la chaine. Si aucun gestionnaire d'événement n'est configuré, la racine (stage) recevra l'événement et le gèrera.

## Gestion des événements

### Filters

Les filtres s'exécutent pendant l'opération de capture. Les filtres permettent d'être avisée qu'une situation se produit et ultimement peut empêcher qu'un événement continue à descendre et soit capturé.

```java

button.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
    @Override
    public void handle(MouseEvent mouseEvent) {
            button.setEffect(shadow);
    }
});
```

### Handlers

Un handler est exécuté pendant la phase de bulle. Si un gestionnaire d'événement pour un node n'a pas consommé l'événement, un node parent pourrait alors décidé de le géré. 

```java
button.setOnAction(new EventHandler<ActionEvent>() {
    @Override
    public void handle(ActionEvent actionEvent) {
        System.out.println(actionEvent);
    }
});
```

La différence principale entre les deux c'est que les filtres peuvent empêcher les handlers d'agir.


