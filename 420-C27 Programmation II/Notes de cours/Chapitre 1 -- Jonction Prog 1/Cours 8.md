# Listener

Habituellement en programmation un EventHandler et un ChangeListener sont très similaires, cependant en JavaFx, il semble avoir séparé les deux concepts.

Donc si l'on comprend bien, un EventHandler comme nous avons vu dernièrement découle d'une intéraction utilisateur (click sur un bouton, mouse over, mouse drag, mouse drop, clé du clavier, ...)

Chaque propriété d'un élément d'interface offre des méthodes d'observation pour déterminer si une de ces valeursObservableList est modifiée. Nous pouvons alors écouter pour des changements par l'entremise du ChangeListener.

Pour un slider par exemple:

```java
// Listen for Slider value changes
slider.valueProperty().addListener(new ChangeListener<Number>() {
	@Override
	public void changed(ObservableValue<? extends Number> observable,
			Number oldValue, Number newValue) {
		
		System.out.println("Nouvelle valeur (newValue: " + newValue.intValue() + ")\n");
	}
});
```

Ça semble un peu complexe, mais comme toujours, c'est relativement la même chose qu'un EventHandler.

* slider.valueProperty() nous donne accès à la propriété value. Il y a plusieurs autres propriétés disponibles tel que focusedProperty ou disabledProperty, toutes deux pourraient être observés à partir d'un ChangeListener.
* addListener(...) s'attend à recevoir un objet de type ChangeLister de type Number. ChangeListener est une interface donc on s'attend à créer un objet qui implémente les besoins de ChangeListener, i-e la méthode changed.
* la méthode changed va se faire appeler à chaque fois qu'un changement à lieu.

