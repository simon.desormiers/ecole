# Interface en javaFX

Plusieurs exemples sur Internet seront basés sur l'utilisation d'un fichier xml, nommé fxml. Des éditeurs d'interfaces tel que gluon permet de générer le contenu des interfaces et de les charger à l'exécution.

Je veux que vous soyez vraiment à l'aise pour créer et surtout rechercher l'information sans utiliser l'interface.

## Pourquoi javaFX?

* Multiplateforme
* Un large éventail de choix d'interface

# Stage

https://jenkov.com/tutorials/javafx/stage.html

![](images/scene_graph.jpg)

Au haut complet de l'architecture il y a un Stage, c'est la représentation JavaFX d'une fenêtre native de système d'exploitation. La seule chose que l'on peut y attacher est une scène.

```java

@Override
    public void start(Stage primaryStage) {
        ...
    }
```

Lors du début du programme, on reçoit en paramètre le stage lors de sa création. C'est sur ce dernier que l'on attache une scène.

```java 
    Scene scene = new Scene (root, 300, 250);
    primaryStage.setScene(scene);
    primaryStage.show();
```

# Scene

https://jenkov.com/tutorials/javafx/scene.html

On crée une scène et on l'attache au stage principal. On remarque aussi le root qui représente l'objet racine de notre application sur lequel se greffera le restant de notre application tel que montrer dans la première image du chapitre.

```java
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.out.println("Hello World");
            }
        });

        StackPane root = new StackPane();
        root.getChildren().add(btn);
```

Toujours dans l'exemple baseProject:

```java
    Button btn = new Button();
    btn.setText("Say 'Hello World'");
    btn.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            System.out.println("Hello World");
        }
    });

    StackPane root = new StackPane();
    root.getChildren().add(btn);
```

## JavaFX Node

C'est la classe de base sur lequel tout le reste des composants découlent que ce soit un composant de positionnement tel que le Split Panel ou un élément d'interface tel qu'un button. C'est une classe abstraite, on ne peut pas créer des objets de types Node, mais toutes les instances d'objets dans la scène partage des propriétés communes définies par la classe Node.

Chaque sous-classe d'instance ne peut être ajoutée à la scène qu'une seule et unique fois, ainsi le même objet ne peut apparaître à plusieurs endroits. Si vous essayez d'ajoutez plusieurs fois le même élément à plusieurs emplacements, JavaFX va lancer une exception.

Certains éléments des Nodes JavaFX peuvent avoir plusieurs enfants: ceux-ci découlent non seulement de la classe Node, mais aussi de la classe Parent.

Les deux objets précédents vu ne font pas partie de la classe Node: stage et scene.

### Propriétés de la classe Node

* Un système de coordonnées cartésiens
* Une boîte délimitante
* layoutX
* layoutY
* Preferred height
* Preferred width
* Minimum height
* Minimum width
* Maximum height
* Maximum width
* User data
* Items (Child nodes)

La liste complète des outils est disponibles ici:

https://jenkov.com/tutorials/javafx/index.html


