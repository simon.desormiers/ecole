# ObservableList

Soit le ComboBox suivant:

```java
    ComboBox<String> emailComboBox = new ComboBox<String>();
    emailComboBox.getItems().addAll("A","B","C","D","E");
    emailComboBox.setValue("A");
    System.out.println(emailComboBox.getValue());
```

Deux éléments importants:

  1. <String> indique le type des objets qui se retrouveront dans le ComboBox, puisqu'ici on utilise String, ce sera des String qui seront à l'intérieur.

  2. Que retourne la méthode getItems selon vous? Il ne s'agit pas juste d'un array d'élément mais bien d'une liste d'élément observée nommée ObservableList.


### Type de sélection

Dans une liste donnée, il est possible de sélectionner un ou plusieurs éléments:

```java

ListView<String> list = new ListView();

// Simple

listview.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

// Plusieurs 

listview.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
```

## Pourquoi utiliser un ObservableList ?

Nous pouvons y rattacher des Listener pour remarquer le changement d'éléments de la liste.

```java
listview.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<String>() {
    @Override
    public void onChanged(Change<? extends String> change) {
        ...
    }
}
```