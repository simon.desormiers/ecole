# Tri par indirection

## Principe 

* Lorsque le tri par indirection est utilisé, le tableau initial demeure inchangé. 
* Créer un tableau d'index qui contiendra selon la clé de tri les index des éléments correspondants. 
* Les données du tableau à trier ne sont plus échangées, seul les tableaux d'index sont triés.

tabIndex =
|       |       |       |       |       |
| :---: | :---: | :---: | :---: | :---: |
|   0   |   1   |   2   |   3   |   4   |
|       |       |       |       |       |

tabChar =
|       |       |       |       |       |
| :---: | :---: | :---: | :---: | :---: |
|  'b'  |  'd'  |  'c'  |  'a'  |  'e'  |
|       |       |       |       |       |

On trie le tableau de données

```
L'ordre devrait être 
a: 3
b: 0
c: 2
d: 1
e: 4
```

Notre tableau devrait donc ressembler à ça

```
tabIndex[0] = 3
tabIndex[1] = 0
tabIndex[2] = 1
tabIndex[3] = 2
tabIndex[4] = 4
````

ou

tabIndex = 
|       |       |       |       |       |
| :---: | :---: | :---: | :---: | :---: |
|   3   |   0   |   1   |   2   |   4   |
|       |       |       |       |       |

