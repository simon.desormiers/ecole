public class Main {

    public static void tri(char[] tab, int[] index){
        for (int i=0; i < tab.length; i++){
            for (int j=0; j < tab.length-1; j++) {
                if (tab[index[i]] < tab[index[j]]) {
                    int temp = index[i];
                    index[i] = index[j];
                    index[j] = temp;
                }
            }
        }
    }

    public static void main(String[] args)
    {
        char [] tab = {'e', 'g', 'd', 'b', 'y', 'p', 'c', 't', 'q'};

        int [] index = new int [tab.length];

        // On initialise la liste des index
        for (int i = 0; i < tab.length; i++){
            index[i] = i;
        }

        System.out.println("Voici le tableau des index");
        for (int i = 0; i < index.length; i++){
            System.out.println(index[i]);
        }

        tri(tab, index);

        System.out.println("Voici le nouveau tableau des index");
        for (int i = 0; i < index.length; i++){
            System.out.println(index[i]);
        }

        System.out.println("Voici le tableau de données");
        for (int i = 0; i < tab.length; i++){
            System.out.println(tab[i]);
        }

        System.out.println("Voici le tableau de données triées");
        for (int i = 0; i < index.length; i++){
            System.out.println(tab[index[i]]);
        }
    }
}