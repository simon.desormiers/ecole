import java.util.Arrays;

public class Main {

    public static void Tableau1D () {
        int[] t1 = new int[]{0, 1, 2, 3};
        char[] t2 = {'2','3'};
        String[] t3 = {"Bonjour", "les", "amis"};

        // à proscrire en utilisation car on a aucune information sur les différents objets de l'array
        // Donc, chaque objet devra être transtypé.
        Object[] t4 = {12, "allo", 'C'};


        // Contenu
        System.out.println(Arrays.toString(t1));
        System.out.println(Arrays.toString(t2));
        System.out.println(Arrays.toString(t3));
        System.out.println(Arrays.toString(t4));

        // Emplacement
        System.out.println(t1);
        System.out.println(t2);
        System.out.println(t3);
        System.out.println(t4);

        System.out.println("" + t2);
    }

    public static void Tableau2D(){
        int[][] tab1 = new int[2][3];
        char[][] tab2 = new char[2][3];

        tab1[0][2] = 3;

        int[][] tab3 = {
                {2,4,6},
                {1,3,5}
        };

        // tab3 est composé de 2 rangées de 3 éléments
        // tab3 ->    0  ->  2 | 4 | 6
        //            1  ->  1 | 3 | 5

        int nbLignes = tab3.length;
        int nbColonnes = tab3[0].length;

        // Syntaxe C
        for (int iLig = 0; iLig < nbLignes; iLig++)
            for (int iCol = 0; iCol < nbColonnes; iCol++)
                System.out.println(tab3[iLig][iCol] + " ");

        // Syntaxe foreach
        for (int[] tabLigne: tab3)
            for (int elem: tabLigne)
                System.out.println(elem);
    }

    public static void main(String[] args) {

        //Tableau1D();
        Tableau2D();

    }
}