public class Utils {
    public static int somme(int n1, int n2) {
        if (n1 == Integer.MAX_VALUE || n2 == Integer.MAX_VALUE)
            return 0;

        return n1 + n2;
    }

    public static double division(double n1, double n2){
        if (n2 != 0)
            return n1 / n2;
        else
            return 0;
    }
}
