module com.example.javafxsimpleproject {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.javafxsimpleproject to javafx.fxml;
    exports com.example.javafxsimpleproject;
}