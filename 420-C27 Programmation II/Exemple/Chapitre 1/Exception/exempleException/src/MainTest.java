import static org.junit.jupiter.api.Assertions.*;
class MainTest {

    @org.junit.jupiter.api.Test
    void somme1() {
        Exception exception = assertThrows(Exception.class, () -> {
                Main.somme(Integer.MAX_VALUE, 1);
        });

        String expectedMessage = "Dépassement de capacité";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @org.junit.jupiter.api.Test
    void somme2() {
        Exception exception = assertThrows(Exception.class, () -> {
            Main.somme(Integer.MIN_VALUE, -1);
        });

        String expectedMessage = "Dépassement de capacité";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
}